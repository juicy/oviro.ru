$(document).ready(() ->
  carusel '.b_catalog_new', 4, 179, 'a.l', 'a.r', 'ul', 3000, false, 'left'
  carusel '.b_show .modif', 4, 48, 'a.l', 'a.r', 'ul', 3000, false, 'left'
  b_search_folding '.b_search'
  b_catalog_elem '.b_catalog_elem'
  tabs '.b_show_tabs', 'ul.tabs a', '.contents .content'
  $("#phone_mask").mask "(999) 999-99-99"
  popups()
  number_control()
)

number_control = () ->
  dr = '[data-role="number_control"]'
  dd = '[data-role="down"]'
  du = '[data-role="up"]'
  d_max = 'data-max'
  d_min = 'data-min'
  i = 'input'

  to = (control, to_val, max, min) ->
    to_val = min if to_val < min
    to_val = max if to_val > max
    input = $(control).find(i)
    input.val(to_val)
    return to_val

  $(dr).each ->

    control = this;

    val = parseInt $(this).find(i).val()
    max = parseInt $(this).find(i).attr(d_max)
    min = parseInt $(this).find(i).attr(d_min)

    $(this).find(dd).click ->
      val = to control, val - 1, max, min
      false

    $(this).find(du).click ->
      val = to control, val + 1, max, min
      false

popups = () ->
  dp = '[data-role="popup"]'
  pops = $(dp)
  pops_open = $('[data-role="popup_open"]')
  pops_close = $('[data-role="popup_close"]')
  wind = '[data-role="popup_window"]'

  $(document).keyup((e) ->
    if e.keyCode == 27 && $(".open#{dp}").length > 0
      close(".open#{dp}")
  )

  open = (pop) ->
    window.location.hash = pop.replace('#', '')
    $(pop).css('display', 'block')
    $(pop).addClass('open')
    $('body').css('overflow', 'hidden')

  close = (pop) ->
    history.pushState('', document.title, window.location.pathname);
    $(pop).css('display', 'none')
    $(pop).removeClass('open')
    $('body').css('overflow', 'auto')

  $('body').click ->
    if $(".open#{dp}").length > 0
      close(".open#{dp}")

  $(pops).each -> 

    id = $(this).attr('id')

    hash = window.location.hash.replace('#', '')
    if hash != '' && hash == id
      open("##{id}")

    $(this).find(wind).click((e) ->
      e.stopPropagation();
    )

  $(pops_open).each ->
    $(this).click ->
      target = $(this).attr('data-target')
      if $(target).length > 0
        open(target)
      false

  $(pops_close).each ->
    $(this).click ->
      target = $(this).parents(dp)
      if $(target).length > 0
        close(target)
      false

tabs = (blocks, labels, contents) ->
  $(blocks).each ->
    block = this
    label_el = $(block).find(labels)
    conte_el = $(block).find(contents)
    $(label_el).click ->
      $(label_el).parents('li').removeClass('active')
      $(this).parents('li').addClass('active')
      $(conte_el).removeClass('active')
      $(block).find("#{contents}:eq(" + $(this).parents('li').prevAll().length + ")").addClass('active')
      false

b_catalog_elem = (blocks) ->
  $(blocks).each ->
    block = this
    $(block).find('.to_market a').click ->
      $(this).addClass('added')
      false

b_search_folding = (block) ->
  $(block).find('label.h').click ->
    $(this).parents('.field').toggleClass('closed')
    false

carusel = (block, in_window, width, left, right, wrap, time, points, napr) ->

  th = 0
  max = $(block).find("ul li").length - in_window
  hover = false

  auto = ->
    to th + 1  unless hover
    setTimeout (->
      auto()
    ), time

  to = (num) ->

    num = max if num < 0
    num = 0 if num > max

    if napr is "top"
      $(block).find(wrap).animate
        "margin-top": num * -1 * width
      , 500, ->
        th = num

    if napr is "left"
      $(block).find(wrap).animate
        "margin-left": num * -1 * width
      , 500, ->
        th = num

    if points
      $(block).find("#{points} a").removeClass("active").addClass("passive")
      $(block).find("#{points} a:eq(#{num})").removeClass("passive").addClass("active")

  setTimeout (->
    auto()
  ), time

  $(block).hover (->
    hover = true
  ), ->
    hover = false

  $(block).find(left).click ->
    to th - 1
    false

  $(block).find(right).click ->
    to th + 1
    false

  if points
    $(block).find("#{points} a").click ->
      n = $(this).prevAll().length
      to n
      false